const expect = require('chai').expect;
const { additionner, soustraire, multiplier } = require('../src/calc');


describe("Additionner", () => {
    it("additionner deux nombres", () => {
        expect(additionner(1, 2)).to.equal(3);
    })
    it("additionner deux nombres à virgule", () => {
        expect(additionner(1.5, 2.5)).to.equal(4);
    })
    it("Retourner une erreur si la réponse est un string", () => {
        expect(additionner("a", "b")).to.contain("Error!");
    })
    it("Retourner une erreur si la réponse est un array", () => {
        expect(additionner([1, 2])).to.contain("Error!");
    })
    it("Retourner une erreur si les paramètres sont nul", () => {
        expect(additionner()).to.contain("Error!");
    })
    it("Retourner une erreur si le premier paramètre est good et que le second est nul", () => {
        expect(additionner(1,)).to.contain("Error!");
    })
    it("Retourner une erreur si le premier paramètre est un number et le second un array", () => {
        expect(additionner(1, [2])).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres sont des arrays", () => {
        expect(additionner([1], [2])).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres sont des booleans", () => {
        expect(additionner(true, false)).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres sont des objets", () => {
        expect(additionner({ a: 1 }, { b: 2 })).to.contain("Error!");
    })
    it("Retourner une erreur si le premier paramètre est un number et le second un objet", () => {
        expect(additionner(1, { b: 2 })).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres ont des exposants infini", () => {
        expect(additionner(0.7, 0.1)).to.equal(0.8);
    })
})

describe("Soustraire", () => {
    it("soustraire deux nombres", () => {
        expect(soustraire(3, 2)).to.equal(1);
    })
    it("soustraire deux nombres à virgule", () => {
        expect(soustraire(5.5, 2.5)).to.equal(3);
    })
    it("Retourner une erreur si la réponse est un string", () => {
        expect(soustraire("a", "b")).to.contain("Error!");
    })
    it("Retourner une erreur si la réponse est un array", () => {
        expect(soustraire([1, 2])).to.contain("Error!");
    })
    it("Retourner une erreur si les paramètres sont nul", () => {
        expect(soustraire()).to.contain("Error!");
    })
    it("Retourner une erreur si le premier paramètre est good et que le second est nul", () => {
        expect(soustraire(1,)).to.contain("Error!");
    })
    it("Retourner une erreur si le premier paramètre est un number et le second un array", () => {
        expect(soustraire(1, [2])).to.contain("Error!");
    })
    it("Retourner une erreur si les paramètres sont des arrays", () => {
        expect(soustraire([1], [2])).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres sont des booleans", () => {
        expect(soustraire(true, false)).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres ont des exposants infini", () => {
        expect(soustraire(0.7, 0.1)).to.equal(0.6);
    })
})

describe("Multiplier", () => {
    it("multiplier deux nombres", () => {
        expect(multiplier(3, 2)).to.equal(6);
    })
    it("multiplier deux nombres à virgule", () => {
        expect(multiplier(3.5, 2.5)).to.equal(8.75);
    })
    it("Retourner une erreur si la réponse est un string", () => {
        expect(multiplier("a", "b")).to.contain("Error!");
    })
    it("Retourner une erreur si les paramètres sont vides", () => {
        expect(multiplier()).to.contain("Error!");
    })
    it("Retourner une erreur si les paramètres sont des arrays", () => {
        expect(multiplier([1], [2])).to.contain("Error!");
    })
    it("Retourner une erreur si il y a qu'un paramètre", () => {
        expect(multiplier(1)).to.contain("Error!");
    })
    it("Retourner une erreur si les deux paramètres sont des booleans", () => {
        expect(multiplier(true, false)).to.contain("Error!");
    })
})