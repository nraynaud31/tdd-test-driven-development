// First extend the native Number object to handle precision. This populates
// the functionality to all math operations.
const additionner = (a, b) => {
    if (typeof (a) != "number" || typeof (b) != "number") {
        return "Error!"
    }
    let la = a.toString().split(".").length;
    let lb = b.toString().split(".").length;
    return la > lb ? (a * (10 * la) + b * (10 * la)) / (la * 10) : (a * (10 * lb) + b * (10 * lb)) / (lb * 10)
}

exports.additionner = additionner;

const soustraire = (a, b) => {
    if (typeof (a) != "number" || typeof (b) != "number") {
        return "Error!"
    }
    let la = a.toString().split(".").length;
    let lb = b.toString().split(".").length;
    return la > lb ? (a * (10 * la) - b * (10 * la)) / (la * 10) : (a * (10 * lb) - b * (10 * lb)) / (lb * 10)
}
exports.soustraire = soustraire;

const multiplier = (a, b) => {
    if (typeof (a, b) != "number") {
        return "Error!"
    }
    return a * b
}

exports.multiplier = multiplier;
